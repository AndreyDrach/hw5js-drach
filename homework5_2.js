let userfirstName = prompt("Введите имя");
let userlastName = prompt("Введите фамилию");
let userbirthday = prompt("Введите дату рождения");

createNewUser = () => {
  let newUser = {
    firstName: userfirstName,
    lastName: userlastName,
    birthday: userbirthday,
    getLogin: function () {
      firstName = this.firstName.toLowerCase();
      lastName = this.lastName.toLowerCase();
      return firstName.slice(0, 1) + lastName;
    },
    getAge: function () {
      let today = new Date();
      let dd = String(today.getDate()).padStart(2, "0");
      let mm = String(today.getMonth() + 1).padStart(2, "0");
      let yyyy = today.getFullYear();

      let birthdayYear = +this.birthday.slice(6, 10);
      let birthdayMonth = +this.birthday.slice(3, 5);
      let birthdayDay = +this.birthday.slice(0, 2);

      let differenceMonth = mm-birthdayMonth;
      let differenceDay = dd-birthdayDay;
      let getAge = yyyy - birthdayYear;
      if (differenceMonth <0)  {
        getAge = getAge - 1;
      }
      if (differenceMonth == 0)  if (differenceDay < 0) {
        getAge = getAge - 1;
      }
      return getAge;
    },
    getPassword: function () {
      let getPasswordName = this.firstName.toUpperCase();
      let getPasswordLastName = this.lastName.toLowerCase();
      let passwordYear = +this.birthday.slice(6, 10);
      return getPasswordName.slice(0, 1) + getPasswordLastName + passwordYear;
    },
  };
  let user = {
    login: newUser.getLogin(),
    age: newUser.getAge(),
    password: newUser.getPassword(),
  };
  return user;
};

console.log(createNewUser());
