// 1. Опишите своими словами, что такое экранирование, и зачем оно нужно в языках программирования
//Экранирование символов означает, что мы делаем что-то с ними, чтобы убедиться, что они распознаются как текст, а не часть кода.

let userfirstName = prompt("Введите имя");
let userlastName = prompt("Введите фамилию");
let userbirthday = prompt("Введите дату рождения");

function createNewUser() {
  let newUser = {
    firstName: userfirstName.toLowerCase(),
    lastName: userlastName.toLowerCase(),
    birthday: userbirthday,
  };
  return (getLogin = newUser.firstName.slice(0, 1) + newUser.lastName);
}

let today = new Date();
let dd = String(today.getDate()).padStart(2, "0");
let mm = String(today.getMonth() + 1).padStart(2, "0");
let yyyy = today.getFullYear();

let birthdayYear = +userbirthday.slice(6, 10);
let birthdayMonth = +userbirthday.slice(3, 5);
let birthdayDay = +userbirthday.slice(0, 1);

let getAge = yyyy - birthdayYear;
if (mm - birthdayMonth < 0) {
  getAge = getAge - 1;
}
if (mm - birthdayMonth == 0 && dd - birthdayDay < 0) {
  getAge = getAge - 1;
}

let getPasswordName = userfirstName.toUpperCase();
let getPasswordLastName = userlastName.toLowerCase();
let getPassword = getPasswordName.slice(0, 1) + getPasswordLastName;

console.log(`createNewUser ${createNewUser()}`);
console.log(`getAge ${getAge}`);
console.log(`getPassword ${getPassword}`);
